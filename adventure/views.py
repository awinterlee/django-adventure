from django.shortcuts import render, get_object_or_404
from.models import Room

def display_room(request,room_name):
    room = get_object_or_404(Room,room_name=room_name)
    context = {
        "room_object": room,
    }
    return render(request, "rooms/room.html", context)
