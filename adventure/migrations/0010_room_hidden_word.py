# Generated by Django 4.1.7 on 2023-02-27 22:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('adventure', '0009_room_room_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='room',
            name='hidden_word',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
